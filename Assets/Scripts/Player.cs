﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	public int moveSpeed;
	public int jumpHeight;
	public Transform groundCheck;
	public LayerMask whatIsGround;
	public float groundCheckRadius;
	public AudioClip jump;
	public AudioClip done;
	public AudioClip getLife;
	public AudioClip hurt;
	public AudioClip die;
	public AudioClip gameOver;
	public Text lifeText;
	public int playerHealth = 2;
	public int attackPower = 1;
	public int playerLives = 3;
	public LevelManager levelManager;
	public GameObject player;
	
	private int addLife = 1;
	private float moveVelocity;
	private bool grounded;
	private Animator animator;
	private bool doubleJumped;
	private LayerMask collisionLayer;

	void Start () {
		collisionLayer = LayerMask.GetMask ("Collision Layer");
		GetComponent<Rigidbody2D>().freezeRotation = true;
		animator = GetComponent<Animator>();
		lifeText = GameObject.Find ("Life Text").GetComponent<Text>();
		lifeText.text = "Life: " + playerLives;
		levelManager = FindObjectOfType<LevelManager>();
	}

	void FixedUpdate () {
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
		animator.SetBool("onGround", grounded);
	}

	void Update () {

		CheckForGameOver();

		moveVelocity = 0f;

		if(Input.GetKeyDown (KeyCode.UpArrow) || Input.GetKeyDown (KeyCode.W)) 
		{

			if(grounded)
			{
				Jump();
				doubleJumped = false;
				SoundController.Instance.PlaySingle(jump);
			}
			else if(!grounded && !doubleJumped)
			{
				Jump();
				doubleJumped = true;
				SoundController.Instance.PlaySingle(jump);
			}

		}

		else if(Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) 
		{
			moveVelocity = moveSpeed;
			animator.SetTrigger("playerWalk");
		}

		else if(Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) 
		{
			moveVelocity = -moveSpeed;
			animator.SetTrigger("playerWalk");
		} 

		else if(Input.GetKey (KeyCode.RightShift) || Input.GetKey (KeyCode.Space))
		{
			animator.SetTrigger("playerAttack");
		}

		else 
		{
			animator.SetTrigger("playerIdle");
		}

		GetComponent<Rigidbody2D>().velocity = new Vector2(moveVelocity, GetComponent<Rigidbody2D>().velocity.y);

		if(GetComponent<Rigidbody2D>().velocity.x > 0) 
		{
			transform.localScale = new Vector3(2.5f, 2.5f, 1f);
		}
		else if(GetComponent<Rigidbody2D>().velocity.x < 0) 
		{
			transform.localScale = new Vector3(-2.5f, 2.5f, 1f);
		}
	}

	public void Jump() {
		GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
	}

	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith) {
		if (objectPlayerCollidedWith.tag == "Exit") {
			SoundController.Instance.music.Stop();
			SoundController.Instance.PlaySingle(done);
		} 
		else if(objectPlayerCollidedWith.tag == "Extra Life") {
			playerLives += addLife;
			lifeText.text = "Life: " + playerLives;
			objectPlayerCollidedWith.gameObject.SetActive(false);
			SoundController.Instance.PlaySingle(getLife);
		} 
	}

	public void CheckForGameOver() {
		if (playerLives <= 0) {
			levelManager.GameOver();
		}
	}

}
