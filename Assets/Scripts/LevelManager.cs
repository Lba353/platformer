﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

	public GameObject startPosition;
	private GameObject gameOverImage;
	private Text overText;

	private Player player;
	private GameObject HUD;
	private GameObject scoreboard;

	void Start () {
		player = FindObjectOfType<Player>();
		HUD = GameObject.Find ("HUD");
		HUD.SetActive (false);
		scoreboard = GameObject.Find ("Scoreboard");
		scoreboard.SetActive (true);
	}

	void Update () {
	
	}

	public void RespawnPlayer () {
		player.transform.position = startPosition.transform.position;
		player.playerLives--;
		player.lifeText.text = "Life: " + player.playerLives;
	}

	public void GameOver() {
		SoundController.Instance.music.Stop();
		scoreboard.SetActive (false);
		HUD.SetActive (true);
		gameOverImage = GameObject.Find("Game Over");
		overText = GameObject.Find("Over Text").GetComponent<Text>();
		overText.text = "Game Over...";
		enabled = false;
	}
}
